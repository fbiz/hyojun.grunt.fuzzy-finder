module.exports = function(grunt){

	grunt.registerMultiTask('fuzzy-finder', function(task){

		var self = this.target;
		var request = require('request');
		var ch = require('cheerio');
		var result = [];
		var data = this.data;
		var done = this.async();


		function getDna(value){
			var result="", prop;
			for (prop in value) { result += "@"+prop+":"+value[prop]+"@"; }
			return result;
		}

		function hasBlob (blob) {
			if (!blob){
				return false;
			}
			var dna = getDna(blob);
			return result.some(function (value) {
				return dna === getDna(value);
			});
		}

		function addBlob (blob) {
			if((!!blob.c && !!blob.c.length) && hasBlob(blob) === false) {
				result.push(blob);
			}
		}

		function getBlob(){
			return {
				c : null,
				t : null,
				h : null
			};
		}

		function parsePage($, url){

			var regex  = new RegExp(grunt.option('host'));
			url = url.replace(regex, "");
			url = (url.substring(0,1) !== '/') ? '/' + url : url;

			var rule, fn = function(index, ele){
				var blob = getBlob();
				blob.h = url;
				blob.t = rule;
				if ( matcher.get($(ele), blob) ) {							
					addBlob(blob);
				}
			}, 
			 nonFn = function(){
				var blob = getBlob();
				blob.h = url;
				blob.t = rule;
				blob.c = $(this).attr(matcher.get);
				addBlob(blob);
			};

			for (rule in data.rules){
				matcher = data.rules[rule];
				if (typeof matcher.get === 'function'){
					$(matcher.query).each( fn );
				} else {
					$(matcher.query).each( nonFn );
				}
			}
			pickURL();
		}

		function fetchPage (url){

			if (grunt.option('verbose')){
				grunt.log.writeln('fetching\t'+url);
			}

			request(url, function(error, resp, body){
				if (error) {
					grunt.fail.fatal("couldn't open page: " + url);
				}
				if ( !!resp && resp.statusCode !== 200){
					grunt.fail.fatal("couldn't open page: " + url + " => status: "+ resp.statusCode);
				} else {
					parsePage(ch.load(body), url);
				}
			});
		}

		function pickURL(){
			var queue = grunt.config.get('fuzzy-finder.'+self+'.pages');
			if (queue.length === 0) {
				if (typeof data.out === 'function'){
					data.out(result, data);
				} else if (typeof data.out === 'string'){
					grunt.file.write( data.out, [
						"(function(db){",
							"try{",
								"require('hyojun.fuzzy-finder/controller')(db);",
							"} catch(err){ console.warn('failed to init hyojun.fuzzy-finder'); console.info(err); }",
						"})("+JSON.stringify(result)+")"].join('\n')
					);
				}
				if (grunt.option('verbose')){
					grunt.log.ok('+ found ' + result.length + ' results.');
				}
				done();
				return;
			}
			
			fetchPage(queue.shift());
			
			grunt.config.set('fuzzy-finder.'+self+'.pages', queue);
		}

		pickURL();

	});
};
