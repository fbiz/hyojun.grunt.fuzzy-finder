#hyojun.grunt.fuzzy-finder

##About

The *hyojunt.grunt.fuzzy-finder* is the [grunt task](http://gruntjs.com/api/grunt.task) used by [hyojun.fuzzy-finder](https://bitbucket.org/fbiz/hyojun.fuzzy-finder) project. 

It's designed to fetch from a list of urls the values you find useful on your project.

##Task config.

The config object must follow this pattern:

```javascript
{
	'fuzzy-finder' : {
		'my-local-cache' : {
			'out' : 'assets/dist/js/hyojun.fuzzy-finder-plugin.js',
			'pages' : ['page-a','page-b'],
			'rules' : {
				'css' : {
					'query' : '.myClassToBeCached',
					'get' : 'class'
				},
				'data-service' : {
					'query' : 'form.ajax',
					'get' : 'action'
				}
			}
		},
		'my-remote-cache' : {
			...
		}
	}
}
```

### fuzzy-finder

```Object```

This task requires a top level config object called: ```fuzzy-finder```

The siblings of this task must be another object (**target**).

> It's a multitask, many targets can be defined and called via ```task:target``` on command line. If no target is provided, the task runs over all targets.

### target.out

```String``` or ```Function```

#### When:

* **String** - since this project is designed to work with [hyojun.fuzzy-finder](https://bitbucket.org/fbiz/hyojun.fuzzy-finder/) the standard ```out``` must be a file path where the cache + plugin initialization will be created.

```javascript
'fuzzy-finder' : {
	'guideline' : {
		'out' :  'dist/js/fuzzy-finder-plugin.js'
	}
}
```
	
The generated file seems like:

```javascript
(function(db){
try{
	require('hyojun.fuzzy-finder/controller')(db);
} catch(err){ console.warn('failed to init hyojun.fuzzy-finder');console.info(err); }
})
([{"c":"Foo","t":"link","h":"my/foo/project"},{"c":"Bar","t":"link","h":"my/foo/project"}]);
```

* **Function** - if your project runs different standards or you can pipe the result with another task/output by providing a function instead

```javascript
'fuzzy-finder' : {
	'guideline' : {
		'out' : function(sampleList){
			//whatever you want
		}
	}
}
```

### target.pages

```Array```

The list of pages (```String```) you want to cache.

### target.rules

```Object```

This is the core of the task. 

Rules is a wrapper object where you place another objects to define what will be cached in. For every rule matching the page, a **sample** will be created.

To understand the rules you first need to understand the **samples** that the task will create:

**Every sample object** contain these properties:

| name | used for |
| -- | -- |
| **sample.h** | stores the 'href' or the url where that sample was taken.
| **sample.c** | stores sample's content, where the search will be performed in. 
| **sample.t** | stores sample type (rule's **name**). Type is a important part of the sample because you can filter your query by 'type'. 

The rule's object must follow this pattern:

```javascript
'rule-name' : {
	'query' : String
	'get' : String|Function
}
```

Where:

* **query** is the selector that will be run on the page the grab the items you want.
* **get** is what you want to get from the item your selector matched. By default (```String```) for each element on your query a **sample.c** will be filled with ```$ele.attr(rule.get)```.

From now on, let's use the following example to illustrate the config:

```javascript
...
'pages' : ['foo/page.html'],
'rules' : {
	'css' : {
		'query' : '.gl-button',
		'get' : 'class',
	},
	'data-plugin'  : {
		'query' : '[data-plugin]',
		'get' : function($ele, sample){
			// $ele is the cheerio instance of the items your 'query' matched
			// here it's a button with text 'send'
			sample.c = $ele.text() + " - " + $ele.attr('data-plugin');
			return sample;
		},
	}
}
...
```

This means that for every page (**config.pages**) the task get it will run the query ``'.gl-button'`` and for **each result** the task will create a **new sample** data.

The example above could created this list:
	
```javascript
[{h:'foo/page.html',c:'gl-button',t:'css'},...
{h:'foo/page.html',c:'send - my-js-plugin',t:'data-plugin'}]
```

###Suggestion for Hyojun.Guideline

The [gruntfile.js](https://bitbucket.org/fbiz/hyojun.grunt.fuzzy-finder/src) on this project has the basic setting to cache everything we consider important on **Hyojun.Guideline**.

##What the task does?

This task will run over all pages listed on ```config.pages``` ```Array```.

For every page the task fetches a [cheerio](https://www.npmjs.com/package/cheerio) instance is created with the whole document. After that the task run through ```config.rules``` object and start caching the values according to rules you created.

##How to install

###For dev

1. Make sure you have [grunt](http://gruntjs.com/) installed (this project is being developed with grunt-cli v0.1.13).

2. `git clone git@bitbucket.org:fbiz/hyojun.grunt.fuzzy-finder.git`

3. create your **gruntfile.js** (you can start with the one provided by this project) and define the rules you want to cache.
	
3. run `grunt` and you will see a file `fuzzy-finder-plugin.js` (suggested task's **out**) being created. This file contains all the samples you cached + AMD call for **hyojun.fuzzy-finder controller**

###Adding this feature on existent projects

1. `npm install https://bitbucket.org/fbiz/hyojun.grunt.fuzzy-finder/get/0.1.4.tar.gz --save-dev`
2. load the task on your gruntfile.js `grunt.loadTasks('node_modules/hyojun.grunt.fuzzy-finder/tasks');`
3. create your `fuzzy-finder` setup inside your grunt config.
4. `grunt fuzzy-finder`

##Road map

1. allow `pages` to a `String` pointing to *sitemap.xml* like;
2. more helpers to write rules without the need of functions (allow getting values not only via **attr()**);
