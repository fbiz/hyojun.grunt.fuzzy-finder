module.exports = function(grunt) {
	"use strict";

	grunt.initConfig({
		'path': {
			'host' : grunt.option('host') || ''
		},
		'fuzzy-finder' : {
			'guideline' : {
				'out' : 'fuzzy-finder-plugin.js',
				'pages' : ['<%= path.host %>/guideline/index.jsp','<%= path.host %>/guideline/pages/mockup.jsp'],
				'rules' : {
					'link' : 	{
						'query' : '.gl-mockup-link, h2.gl, h3.gl, h4.gl, h5.gl, h6.gl',
						'get' : function($ele, blob){
							var id = $ele.attr("id");
							if ($ele.get(0).name === 'a'){
								blob.h = $ele.attr('href');
							} else if (!!id && id.length){
								blob.h = blob.h + "#" + id;
							}
							blob.c = $ele.text().replace(/[\n|\t]/,"");
							return blob;
						}
					},
					'css' : {
						'query' : '.gl-example-full, .gl-example',
						'get' : function($ele, blob){
							var css = $ele.children(0).attr('class');
							var prev = $ele.prev("h3.gl");
							if (prev.hasClass('gl')){
								css = prev.text() + ' - .' + css;
								blob.h = blob.h + "#" + prev.attr("id");
							}
							blob.c = css;
							return blob;
						}
					},
					// to look inside the pages and find any variation.
					'insertPages' : {
						'query' : '.gl-mockup-link',
						'get' : function($ele, blob){
						 	if (blob.h.indexOf("guideline/pages/mockup.jsp") != -1 ){
						 		if (!!$ele.attr('href')){
									var list = grunt.config.get('fuzzy-finder.guideline.pages');
									list.push(grunt.config.get('path.host') + $ele.attr('href'));
									grunt.config.set('fuzzy-finder.guideline.pages', list);
							 	}
							 }
							return undefined;
						}
					},
					'template' : {
						'query' : 'script[type="text/x-mustache-template"]',
						'get' : 'id'
					},
					'data-service' : {
						'query' : '[data-service]',
						'get' : 'data-service'
					},
					'data-plugin' : {
						'query' : '[data-plugin]',
						'get' : 'data-plugin'
					}
				}
			}
		}
	});
	grunt.loadTasks('tasks');
	grunt.registerTask('default',['fuzzy-finder']);
};

